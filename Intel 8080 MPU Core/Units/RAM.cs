﻿using Intel_8080_MPU_Core.Models;

namespace Intel_8080_MPU_Core.Units
{
    public class RAM : IRAM
    {
        byte[] Memory;
        public ulong Size { get; set; }
        public RAM(uint size)
        {
            Memory = new byte[size];
            Size = size;
        }
        public RAM(ushort size)
        {
            Memory = new byte[size];
            Size = size;
        }
        public RAM(ulong size)
        {
            Memory = new byte[size];
            Size = size;
        }

        public RAM(int size)
        {
            Memory = new byte[size];
            Size = (ulong)size;
        }

        public RAM(short size)
        {
            Memory = new byte[size];
            Size = (ulong)size;
        }

        public RAM(long size)
        {
            Memory = new byte[size];
            Size = (ulong)size;
        }

        public byte ReadByte(ushort address)
        {
            return Memory[address];
        }

        public byte ReadByte(ulong address)
        {
            return Memory[address];
        }

        public byte ReadByte(uint address)
        {
            return Memory[address];
        }

        //public short ReadShort(short address)
        //{
        //    return (short)(((0x0000 | Memory[address]) << 8) | Memory[address + 1]);
        //}

        //public byte[] ReadTwoBytes(short address)
        //{
        //    return new byte[] { Memory[address], Memory[address + 1] };
        //}

        public void WriteByte(ushort address, byte data)
        {
            Memory[address] = data;
        }

        public void WriteByte(ulong address, byte data)
        {
            Memory[address] = data;
        }

        public void WriteByte(uint address, byte data)
        {
            Memory[address] = data;
        }

        //public void WriteShort(short address, short data)
        //{
        //    Memory[address] = (byte)(data >> 8);
        //    Memory[address + 1] = (byte)(data & 0x00FF);
        //}

        //public void WriteTwoBytes(short address, byte[] data)
        //{
        //    Memory[address] = data[0];
        //    Memory[address + 1] = data[1];
        //}
    }
}
