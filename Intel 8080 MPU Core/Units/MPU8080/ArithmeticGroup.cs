﻿namespace Intel_8080_MPU_Core.Units.MPU8080
{
    public partial class MPU8080
    {
        public int ADD(Registers.RegisterEncoding source)
        {
            AddToAccumulator(Registers.GetRegister(source));
            return 4;
        }
        public int ADD()
        {
            AddToAccumulator(EndianIO.ReadByteAt(Registers.HL));
            return 7;
        }
        public int ADI()
        {
            AddToAccumulator(EndianIO.ReadByte());
            return 7;
        }
        public int ADC(Registers.RegisterEncoding source)
        {
            AddToAccumulator(Registers.GetRegister(source), true);
            return 4;
        }
        public int ADC()
        {
            AddToAccumulator(EndianIO.ReadByteAt(Registers.HL), true);
            return 7;
        }
        public int ACI()
        {
            AddToAccumulator(EndianIO.ReadByte(), true);
            return 7;
        }
        public int SUB(Registers.RegisterEncoding source)
        {
            SubstractFromAccumulator(Registers.GetRegister(source));
            return 4;
        }
        public int SUB()
        {
            SubstractFromAccumulator(EndianIO.ReadByteAt(Registers.HL));
            return 7;
        }
        public int SUI()
        {
            SubstractFromAccumulator(EndianIO.ReadByte());
            return 7;
        }
        public int SBB(Registers.RegisterEncoding source)
        {
            SubstractFromAccumulator(Registers.GetRegister(source), true);
            return 4;
        }
        public int SBB()
        {
            SubstractFromAccumulator(EndianIO.ReadByteAt(Registers.HL), true);
            return 7;
        }
        public int SBI()
        {
            SubstractFromAccumulator(EndianIO.ReadByte(), true);
            return 7;
        }
        public int INR(Registers.RegisterEncoding destination)
        {
            byte temp = Registers.GetRegister(destination);
            AuxiliaryCarry(temp, 1);
            temp++;
            Registers.IncreaseRegister(destination);
            ZeroSignParity(temp);
            return 5;
        }
        public int INR()
        {
            byte temp = EndianIO.ReadByteAt(Registers.HL);
            AuxiliaryCarry(temp, 1);
            temp++;
            EndianIO.WriteByte(Registers.HL, temp);
            ZeroSignParity(temp);
            return 10;
        }
        public int DCR(Registers.RegisterEncoding destination)
        {
            byte temp = Registers.GetRegister(destination);
            AuxiliaryCarry(temp, ChangeSign(1));
            temp--;
            Registers.DecreaseRegister(destination);
            ZeroSignParity(temp);
            return 5;
        }
        public int DCR()
        {
            byte temp = EndianIO.ReadByteAt(Registers.HL);
            AuxiliaryCarry(temp, ChangeSign(1));
            temp--;
            EndianIO.WriteByte(Registers.HL, temp);
            ZeroSignParity(temp);
            return 10;
        }
        public int INX(Registers.RegisterPairEncoding registerPair)
        {
            Registers.IncreaseRegisterPair(registerPair);
            return 5;
        }
        public int DCX(Registers.RegisterPairEncoding registerPair)
        {
            Registers.DecreaseRegisterPair(registerPair);
            return 5;
        }
        public int DAD(Registers.RegisterPairEncoding registerPair)
        {
            ushort temp = Registers.GetRegisterPair(registerPair);
            Registers.F.CY = (0xFFFF - Registers.HL) < temp ? (byte)1 : (byte)0;
            Registers.HL += temp;
            return 10;
        }
        public int DAA()
        {
            byte first4Bits = (byte)(Registers.A & 0x0F);
            if ((first4Bits > 9) || Registers.F.AC == 1)
            {
                Registers.F.CY = (255 - Registers.A) < 6 ? (byte)1 : Registers.F.CY;
                Registers.A += 6;
                AuxiliaryCarry(first4Bits, 6);
            }
            byte most4Bits = (byte)((Registers.A & 0xF0) >> 4);
            if ((most4Bits > 9) || Registers.F.CY == 1)
            {
                Registers.F.CY = (15 - most4Bits) < 6 ? (byte)1 : Registers.F.CY;
                Registers.A += 0x60;
            }
            ZeroSignParity(Registers.A);
            return 4;
        }
        void AddToAccumulator(byte data, bool addCarry = false)
        {
            if (addCarry && Registers.F.CY == 1)
            {
                Registers.F.CY = (255 - Registers.A) <= data ? (byte)1 : (byte)0;
                Registers.F.AC = (15 - (Registers.A & 0xF)) <= (data & 0xF) ? (byte)1 : (byte)0;
                Registers.A += data;
                Registers.A++;
            }
            else
            {
                Registers.F.CY = (255 - Registers.A) < data ? (byte)1 : (byte)0;
                AuxiliaryCarry(Registers.A, data);
                Registers.A += data;
            }
            ZeroSignParity(Registers.A);
        }
        void SubstractFromAccumulator(byte data, bool substractBorrow = false)
        {
            if (substractBorrow && Registers.F.CY == 1)
            {
                data = (byte)(ChangeSign(data) - 1);
                Registers.F.AC = (15 - (Registers.A & 0xF)) < (data & 0xF) ? (byte)1 : (byte)0;
                Registers.F.CY = (255 - Registers.A) < data ? (byte)0 : (byte)1;
            }
            else
            {
                Registers.F.AC = (Registers.A & 0xF) < (data & 0xF) ? (byte)0 : (byte)1;
                Registers.F.CY = Registers.A < data ? (byte)1 : (byte)0;
                data = ChangeSign(data);
            }
            Registers.A += data;
            ZeroSignParity(Registers.A);
        }
        void AuxiliaryCarry(byte first, byte second)
        {
            Registers.F.AC = (15 - (first & 0xF)) < (second & 0xF) ? (byte)1 : (byte)0;
        }
        void ZeroSignParity(byte reference)
        {
            Registers.F.Z = reference != 0 ? (byte)0x0 : (byte)0x1;
            Registers.F.S = GetBit(reference, 7);
            Registers.F.Parity(reference);
        }

        byte ChangeSign(byte data)
        {
            return (byte)(~data + 1);
        }
    }
}
