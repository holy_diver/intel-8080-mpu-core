﻿namespace Intel_8080_MPU_Core.Units.MPU8080
{
    public partial class MPU8080
    {
        public enum ConditionFlags
        {
            NZ = 0,
            Z = 1,
            NC = 2,
            C = 3,
            PO = 4,
            PE = 5,
            P = 6,
            M = 7,
            None = 8
        }

        public int JMP()
        {
            Registers.PC = EndianIO.ReadUShort(EndianIO.Endianness.Little);
            return 10;
        }
        public int Jcondition(ConditionFlags condition)
        {   
            if(IsConditionTrue(condition))
            {
                JMP();
                return 10;
            }
            Registers.PC += 2;
            return 10;
        }
        public int CALL()
        {
            ushort nextInstructionAdress = (ushort)(Registers.PC + 2);
            EndianIO.WriteUShort((ushort)(Registers.SP - 2), nextInstructionAdress, EndianIO.Endianness.Little);
            Registers.SP -= 2;
            Registers.PC = EndianIO.ReadUShort(EndianIO.Endianness.Little);
            return 17;
        }
        public int Ccondition(ConditionFlags condition)
        {
            if(IsConditionTrue(condition))
            {
                CALL();
                return 17;
            }
            Registers.PC += 2;
            return 11;
        }
        public int RET()
        {
            Registers.PC = EndianIO.ReadUShortAt(Registers.SP, EndianIO.Endianness.Little);
            Registers.SP += 2;
            return 10;
        }
        public int Rcondition(ConditionFlags condition)
        {
            if(IsConditionTrue(condition))
            {
                RET();
                return 11;
            }
            return 5;
        }
        public int RST(byte number)
        {
            ushort nextInstructionAdress = Registers.PC;
            EndianIO.WriteUShort((ushort)(Registers.SP - 2), nextInstructionAdress, EndianIO.Endianness.Little);
            Registers.SP -= 2;
            Registers.PC = (ushort)(8 * number);
            return 11;
        }
        public int PCHL()
        {
            Registers.PC = Registers.HL;
            return 5;
        }

        bool IsConditionTrue(ConditionFlags condition)
        {
            switch (condition)
            {
                case ConditionFlags.NZ:
                    return Registers.F.Z == 0;
                case ConditionFlags.Z:
                    return Registers.F.Z == 1;
                case ConditionFlags.NC:
                    return Registers.F.CY == 0;
                case ConditionFlags.C:
                    return Registers.F.CY == 1;
                case ConditionFlags.PO:
                    return Registers.F.P == 0;
                case ConditionFlags.PE:
                    return Registers.F.P == 1;
                case ConditionFlags.P:
                    return Registers.F.S == 0;
                case ConditionFlags.M:
                    return Registers.F.S == 1;
                default:
                    throw new System.Exception("No Condition Flag");
            }
        }
    }
}
