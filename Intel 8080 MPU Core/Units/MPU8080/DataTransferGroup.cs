﻿using Intel_8080_MPU_Core.Models;

namespace Intel_8080_MPU_Core.Units.MPU8080
{
    public partial class MPU8080
    {
        [Instruction(Length: 1, Duration: 5, InstructionAttribute.Flags.None)]
        public int MOV(Registers.RegisterEncoding destination, Registers.RegisterEncoding source)
        {
            Registers.SetRegister(destination, Registers.GetRegister(source));
            return 5;
        }

        [Instruction(Length: 2, Duration: 7, InstructionAttribute.Flags.None)]
        public int MOV_rM(Registers.RegisterEncoding destination)
        {
            Registers.SetRegister(destination, EndianIO.ReadByteAt(Registers.HL));
            return 7;
        }

        [Instruction(Length: 2, Duration: 7, InstructionAttribute.Flags.None)]
        public int MOV_Mr(Registers.RegisterEncoding source)
        {
            RAM.WriteByte(Registers.HL, Registers.GetRegister(source));
            return 7;
        }

        [Instruction(Length: 2, Duration: 7, InstructionAttribute.Flags.None)]
        public int MVI(Registers.RegisterEncoding destination)
        {
            Registers.SetRegister(destination, EndianIO.ReadByte());
            return 7;
        }

        [Instruction(Length: 3, Duration: 10, InstructionAttribute.Flags.None)]
        public int MVI()
        {
            EndianIO.WriteByte(Registers.HL, EndianIO.ReadByte());
            return 10;
        }

        [Instruction(Length: 2, Duration: 7, InstructionAttribute.Flags.None)]
        public int LXI(Registers.RegisterPairEncoding registerPair)
        {
            Registers.SetRegisterPair(registerPair, EndianIO.ReadUShort(EndianIO.Endianness.Little));
            return 10;
        }

        [Instruction(Length: 4, Duration: 13, InstructionAttribute.Flags.None)]
        public int LDA()
        {
            Registers.A = EndianIO.ReadByteAt(EndianIO.ReadUShort(EndianIO.Endianness.Little));
            return 13;
        }

        [Instruction(Length: 4, Duration: 13, InstructionAttribute.Flags.None)]
        public int STA()
        {
            EndianIO.WriteByte(EndianIO.ReadUShort(EndianIO.Endianness.Little), Registers.A);
            return 13;
        }

        [Instruction(Length: 5, Duration: 16, InstructionAttribute.Flags.None)]
        public int LHLD()
        {
            Registers.HL = EndianIO.ReadUShortAt(EndianIO.ReadUShort(EndianIO.Endianness.Little), EndianIO.Endianness.Little);
            return 16;
        }

        [Instruction(Length: 5, Duration: 16, InstructionAttribute.Flags.None)]
        public int SHLD()
        {
            EndianIO.WriteUShort(EndianIO.ReadUShort(EndianIO.Endianness.Little), Registers.HL, EndianIO.Endianness.Little);
            return 16;
        }

        [Instruction(Length: 2, Duration: 7, InstructionAttribute.Flags.None)]
        public int LDAX(Registers.RegisterPairEncoding registerPair)
        {
            if (registerPair != Registers.RegisterPairEncoding.BC && registerPair != Registers.RegisterPairEncoding.DE)
                IncorrectRegPairEncoding();
            Registers.A = EndianIO.ReadByteAt(Registers.GetRegisterPair(registerPair));
            return 7;
        }

        [Instruction(Length: 2, Duration: 7, InstructionAttribute.Flags.None)]
        public int STAX(Registers.RegisterPairEncoding registerPair)
        {
            if (registerPair != Registers.RegisterPairEncoding.BC && registerPair != Registers.RegisterPairEncoding.DE)
                IncorrectRegPairEncoding();
            EndianIO.WriteByte(Registers.GetRegisterPair(registerPair), Registers.A);
            return 7;
        }

        [Instruction(Length: 1, Duration: 4, InstructionAttribute.Flags.None)]
        public int XCHG()
        {
            byte temp;
            temp = Registers.H;
            Registers.H = Registers.D;
            Registers.D = temp;
            temp = Registers.L;
            Registers.L = Registers.E;
            Registers.E = temp;
            return 4;
        }

        byte GetBit(byte input, byte index)
        {
            return (byte)((input & 1 << index) >> index);
        }
    }
}
