﻿using Intel_8080_MPU_Core.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Intel_8080_MPU_Core.Units.MPU8080
{
    public partial class MPU8080
    {
        /// <summary>
        /// Default Frequency of 2 MHz or 2000000 cycles per second
        /// </summary>
        const ulong Frequency = 2000000;
        public IRAM RAM;
        public Registers Registers;
        public EndianIO EndianIO;
        public byte[] Ports;
        bool interrupts = false;
        bool isHalted = false;
        bool allowLogging = false;
        bool isWriting = false;
        StreamWriter logStream;
        public bool BdosEnabled = false;
        int instructionsProcessed;

        public MPU8080(IRAM RAM, IFlags Flags, ushort PortCount, bool EnableBDOS, bool allowLogging)
        {
            this.RAM = RAM;
            Registers = new Registers(Flags);
            EndianIO = new EndianIO(this.RAM, Registers);
            Ports = new byte[PortCount];
            BdosEnabled = EnableBDOS;
            if (allowLogging)
                logStream = new StreamWriter(new FileStream("log.txt", FileMode.Create, FileAccess.Write));
            this.allowLogging = allowLogging;
        }

        public void Init(ulong freq = Frequency)
        {
            if (BdosEnabled)
            {
                Registers.PC = 0x100;
                RAM.WriteByte(5, 0xC9);
            }
            Stopwatch stopwatch = new Stopwatch();
            ulong availableCycles;
            while (true)
            {
                availableCycles = freq;
                //stopwatch.Restart();
                //byte lastInstruction = 0;
                while (availableCycles > 0)
                {
                    if (BdosEnabled)
                    {
                        if (Registers.PC == 0x5)
                        {
                            if (Registers.C == 2)
                            {
                                Console.Write(Encoding.ASCII.GetString(new byte[] { Registers.E }));
                                isWriting = true;
                                if (Registers.E == 0xD)
                                    isWriting = false;
                            }
                            else if (Registers.C == 0x9)
                            {
                                for (ushort address = Registers.DE; EndianIO.ReadByteAt(address) != '$'; address++)
                                {
                                    byte b = EndianIO.ReadByteAt(address);
                                    isWriting = true;
                                    if (b == 0xD)
                                        isWriting = false;
                                    if (b != 0)
                                    {
                                        Console.Write(Encoding.ASCII.GetString(new byte[] { b }));
                                    }
                                }
                                ;
                            }
                        }
                    }
                    if (!isWriting && allowLogging)
                    {
                        logStream.WriteLine("--------------------------------------------------------------------------------------------------");
                    }
                    byte instruction = EndianIO.ReadByte();
                    if (!isWriting && allowLogging)
                    {
                        logStream.WriteLine("OPCode: " + BitConverter.ToString(new byte[] { instruction }));
                        logStream.WriteLine("Next Byte: " + EndianIO.ReadByteAt(Registers.PC).ToString("X2"));
                        logStream.WriteLine("Next 2 Bytes: " + EndianIO.ReadUShortAt(Registers.PC, EndianIO.Endianness.Little).ToString("X4"));
                    }
                    availableCycles -= (ulong)InstructionPipeline(instruction);
                    instructionsProcessed++;
                    if (!isWriting && allowLogging)
                        PrintDiag();
                    if (Registers.PC == 0)
                    {
                        logStream.Flush();
                        logStream.Dispose();
                        return;
                    }
                }
                while (stopwatch.ElapsedMilliseconds < 1000) ;
            }
        }

        int InstructionPipeline(byte instruction)
        {
            int cycles = 0;
            byte firstNibble = (byte)((instruction & 0xF0) >> 4);
            byte secondNibble = (byte)(instruction & 0xF);
            Registers.RegisterPairEncoding regPair = (Registers.RegisterPairEncoding)((instruction & 0x30) >> 4);
            Registers.RegisterEncoding destination = (Registers.RegisterEncoding)((instruction & 0x38) >> 3);
            ConditionFlags condition = (ConditionFlags)((instruction & 0x38) >> 3);
            Registers.RegisterEncoding source = (Registers.RegisterEncoding)(instruction & 0x07);
            switch (firstNibble)
            {
                case 0:
                    switch (secondNibble)
                    {
                        case 0x00:
                            cycles = NOP();
                            break;
                        case 0x01:
                            if (regPair != Registers.RegisterPairEncoding.BC)
                                IncorrectRegPairEncoding();
                            cycles = LXI(regPair);
                            break;
                        case 0x02:
                            if (regPair != Registers.RegisterPairEncoding.BC)
                                IncorrectRegPairEncoding();
                            cycles = STAX(regPair);
                            break;
                        case 0x03:
                            if (regPair != Registers.RegisterPairEncoding.BC)
                                IncorrectRegPairEncoding();
                            cycles = INX(regPair);
                            break;
                        case 0x04:
                            if (destination != Registers.RegisterEncoding.B)
                                IncorrectRegister();
                            cycles = INR(destination);
                            break;
                        case 0x05:
                            if (destination != Registers.RegisterEncoding.B)
                                IncorrectRegister();
                            cycles = DCR(destination);
                            break;
                        case 0x06:
                            if (destination != Registers.RegisterEncoding.B)
                                IncorrectRegister();
                            cycles = MVI(destination);
                            break;
                        case 0x07:
                            cycles = RLC();
                            break;
                        case 0x08:
                            NotImplemented();
                            break;
                        case 0x09:
                            if (regPair != Registers.RegisterPairEncoding.BC)
                                IncorrectRegPairEncoding();
                            cycles = DAD(regPair);
                            break;
                        case 0x0A:
                            if (regPair != Registers.RegisterPairEncoding.BC)
                                IncorrectRegPairEncoding();
                            cycles = LDAX(regPair);
                            break;
                        case 0x0B:
                            if (regPair != Registers.RegisterPairEncoding.BC)
                                IncorrectRegPairEncoding();
                            cycles = DCX(regPair);
                            break;
                        case 0x0C:
                            if (destination != Registers.RegisterEncoding.C)
                                IncorrectRegister();
                            cycles = INR(destination);
                            break;
                        case 0x0D:
                            if (destination != Registers.RegisterEncoding.C)
                                IncorrectRegister();
                            cycles = DCR(destination);
                            break;
                        case 0x0E:
                            if (destination != Registers.RegisterEncoding.C)
                                IncorrectRegister();
                            cycles = MVI(destination);
                            break;
                        case 0x0F:
                            cycles = RRC();
                            break;
                    }
                    break;

                case 1:
                    switch (secondNibble)
                    {
                        case 0x0:
                            NotImplemented();
                            break;
                        case 0x1:
                            if (regPair != Registers.RegisterPairEncoding.DE)
                                IncorrectRegPairEncoding();
                            cycles = LXI(regPair);
                            break;
                        case 0x2:
                            if (regPair != Registers.RegisterPairEncoding.DE)
                                IncorrectRegPairEncoding();
                            cycles = STAX(regPair);
                            break;
                        case 0x3:
                            if (regPair != Registers.RegisterPairEncoding.DE)
                                IncorrectRegPairEncoding();
                            cycles = INX(regPair);
                            break;
                        case 0x4:
                            if (destination != Registers.RegisterEncoding.D)
                                IncorrectRegister();
                            cycles = INR(destination);
                            break;
                        case 0x5:
                            if (destination != Registers.RegisterEncoding.D)
                                IncorrectRegister();
                            cycles = DCR(destination);
                            break;
                        case 0x6:
                            if (destination != Registers.RegisterEncoding.D)
                                IncorrectRegister();
                            cycles = MVI(destination);
                            break;
                        case 0x7:
                            cycles = RAL();
                            break;
                        case 0x8:
                            NotImplemented();
                            break;
                        case 0x9:
                            if (regPair != Registers.RegisterPairEncoding.DE)
                                IncorrectRegPairEncoding();
                            cycles = DAD(regPair);
                            break;
                        case 0xA:
                            if (regPair != Registers.RegisterPairEncoding.DE)
                                IncorrectRegPairEncoding();
                            cycles = LDAX(regPair);
                            break;
                        case 0xB:
                            if (regPair != Registers.RegisterPairEncoding.DE)
                                IncorrectRegPairEncoding();
                            cycles = DCX(regPair);
                            break;
                        case 0xC:
                            if (destination != Registers.RegisterEncoding.E)
                                IncorrectRegister();
                            cycles = INR(destination);
                            break;
                        case 0xD:
                            if (destination != Registers.RegisterEncoding.E)
                                IncorrectRegister();
                            cycles = DCR(destination);
                            break;
                        case 0xE:
                            if (destination != Registers.RegisterEncoding.E)
                                IncorrectRegister();
                            cycles = MVI(destination);
                            break;
                        case 0xF:
                            cycles = RAR();
                            break;
                    }
                    break;

                case 2:
                    switch (secondNibble)
                    {
                        case 0x0:
                            NotImplemented();
                            break;
                        case 0x1:
                            if (regPair != Registers.RegisterPairEncoding.HL)
                                IncorrectRegPairEncoding();
                            cycles = LXI(regPair);
                            break;
                        case 0x2:
                            cycles = SHLD();
                            break;
                        case 0x3:
                            if (regPair != Registers.RegisterPairEncoding.HL)
                                IncorrectRegPairEncoding();
                            cycles = INX(regPair);
                            break;
                        case 0x4:
                            if (destination != Registers.RegisterEncoding.H)
                                IncorrectRegister();
                            cycles = INR(destination);
                            break;
                        case 0x5:
                            if (destination != Registers.RegisterEncoding.H)
                                IncorrectRegister();
                            cycles = DCR(destination);
                            break;
                        case 0x6:
                            if (destination != Registers.RegisterEncoding.H)
                                IncorrectRegister();
                            cycles = MVI(destination);
                            break;
                        case 0x7:
                            cycles = DAA();
                            break;
                        case 0x8:
                            NotImplemented();
                            break;
                        case 0x9:
                            if (regPair != Registers.RegisterPairEncoding.HL)
                                IncorrectRegPairEncoding();
                            cycles = DAD(regPair);
                            break;
                        case 0xA:
                            cycles = LHLD();
                            break;
                        case 0xB:
                            if (regPair != Registers.RegisterPairEncoding.HL)
                                IncorrectRegPairEncoding();
                            cycles = DCX(regPair);
                            break;
                        case 0xC:
                            if (destination != Registers.RegisterEncoding.L)
                                IncorrectRegister();
                            cycles = INR(destination);
                            break;
                        case 0xD:
                            if (destination != Registers.RegisterEncoding.L)
                                IncorrectRegister();
                            cycles = DCR(destination);
                            break;
                        case 0xE:
                            if (destination != Registers.RegisterEncoding.L)
                                IncorrectRegister();
                            cycles = MVI(destination);
                            break;
                        case 0xF:
                            cycles = CMA();
                            break;
                    }
                    break;

                case 3:
                    switch (secondNibble)
                    {
                        case 0x0:
                            NotImplemented();
                            break;
                        case 0x1:
                            if (regPair != Registers.RegisterPairEncoding.SP)
                                IncorrectRegPairEncoding();
                            cycles = LXI(regPair);
                            break;
                        case 0x2:
                            cycles = STA();
                            break;
                        case 0x3:
                            if (regPair != Registers.RegisterPairEncoding.SP)
                                IncorrectRegPairEncoding();
                            cycles = INX(regPair);
                            break;
                        case 0x4:
                            cycles = INR();
                            break;
                        case 0x5:
                            cycles = DCR();
                            break;
                        case 0x6:
                            cycles = MVI();
                            break;
                        case 0x7:
                            cycles = STC();
                            break;
                        case 0x8:
                            NotImplemented();
                            break;
                        case 0x9:
                            if (regPair != Registers.RegisterPairEncoding.SP)
                                IncorrectRegPairEncoding();
                            cycles = DAD(regPair);
                            break;
                        case 0xA:
                            cycles = LDA();
                            break;
                        case 0xB:
                            if (regPair != Registers.RegisterPairEncoding.SP)
                                IncorrectRegPairEncoding();
                            cycles = DCX(regPair);
                            break;
                        case 0xC:
                            if (destination != Registers.RegisterEncoding.A)
                                IncorrectRegister();
                            cycles = INR(destination);
                            break;
                        case 0xD:
                            if (destination != Registers.RegisterEncoding.A)
                                IncorrectRegister();
                            cycles = DCR(destination);
                            break;
                        case 0xE:
                            if (destination != Registers.RegisterEncoding.A)
                                IncorrectRegister();
                            cycles = MVI(destination);
                            break;
                        case 0xF:
                            cycles = CMC();
                            break;
                    }
                    break;

                case 4:
                    switch (secondNibble)
                    {
                        case 6:
                        case 0xE:
                            cycles = MOV_rM(destination);
                            break;
                        default:
                            cycles = MOV(destination, source);
                            break;
                    }
                    break;

                case 5:
                    switch (secondNibble)
                    {
                        case 6:
                        case 0xE:
                            cycles = MOV_rM(destination);
                            break;
                        default:
                            cycles = MOV(destination, source);
                            break;
                    }
                    break;

                case 6:
                    switch (secondNibble)
                    {
                        case 6:
                        case 0xE:
                            cycles = MOV_rM(destination);
                            break;
                        default:
                            cycles = MOV(destination, source);
                            break;
                    }
                    break;

                case 7:
                    switch (secondNibble)
                    {
                        case 0xE:
                            cycles = MOV_rM(destination);
                            break;
                        case 6:
                            cycles = HLT();
                            break;
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 7:
                            cycles = MOV_Mr(source);
                            break;
                        default:
                            cycles = MOV(destination, source);
                            break;
                    }
                    break;

                case 8:
                    switch (secondNibble)
                    {
                        case 6:
                            cycles = ADD();
                            break;
                        case 0xE:
                            cycles = ADC();
                            break;
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 7:
                            cycles = ADD(source);
                            break;
                        default:
                            cycles = ADC(source);
                            break;
                    }
                    break;

                case 9:
                    switch (secondNibble)
                    {
                        case 6:
                            cycles = SUB();
                            break;
                        case 0xE:
                            cycles = SBB();
                            break;
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 7:
                            cycles = SUB(source);
                            break;
                        default:
                            cycles = SBB(source);
                            break;
                    }
                    break;

                case 0xA:
                    switch (secondNibble)
                    {
                        case 6:
                            cycles = ANA();
                            break;
                        case 0xE:
                            cycles = XRA();
                            break;
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 7:
                            cycles = ANA(source);
                            break;
                        default:
                            cycles = XRA(source);
                            break;
                    }
                    break;

                case 0xB:
                    switch (secondNibble)
                    {
                        case 6:
                            cycles = ORA();
                            break;
                        case 0xE:
                            cycles = CMP();
                            break;
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 7:
                            cycles = ORA(source);
                            break;
                        default:
                            cycles = CMP(source);
                            break;
                    }
                    break;

                case 0xC:
                    switch (secondNibble)
                    {
                        case 0:
                            if (condition != ConditionFlags.NZ)
                                IncorrectCondition();
                            cycles = Rcondition(condition);
                            break;
                        case 1:
                            if (regPair != Registers.RegisterPairEncoding.BC)
                                IncorrectRegPairEncoding();
                            cycles = POP(regPair);
                            break;
                        case 2:
                            if (condition != ConditionFlags.NZ)
                                IncorrectCondition();
                            cycles = Jcondition(condition);
                            break;
                        case 3:
                            cycles = JMP();
                            break;
                        case 4:
                            if (condition != ConditionFlags.NZ)
                                IncorrectCondition();
                            cycles = Ccondition(condition);
                            break;
                        case 5:
                            if (regPair != Registers.RegisterPairEncoding.BC)
                                IncorrectRegPairEncoding();
                            cycles = PUSH(regPair);
                            break;
                        case 6:
                            cycles = ADI();
                            break;
                        case 7:
                            cycles = RST((byte)condition);
                            break;
                        case 8:
                            if (condition != ConditionFlags.Z)
                                IncorrectCondition();
                            cycles = Rcondition(condition);
                            break;
                        case 9:
                            cycles = RET();
                            break;
                        case 0xA:
                            if (condition != ConditionFlags.Z)
                                IncorrectCondition();
                            cycles = Jcondition(condition);
                            break;
                        case 0xB:
                            NotImplemented();
                            break;
                        case 0xC:
                            if (condition != ConditionFlags.Z)
                                IncorrectCondition();
                            cycles = Ccondition(condition);
                            break;
                        case 0xD:
                            cycles = CALL();
                            break;
                        case 0xE:
                            cycles = ACI();
                            break;
                        case 0xF:
                            cycles = RST((byte)condition);
                            break;
                    }
                    break;

                case 0xD:
                    switch (secondNibble)
                    {
                        case 0:
                            if (condition != ConditionFlags.NC)
                                IncorrectCondition();
                            cycles = Rcondition(condition);
                            break;
                        case 1:
                            if (regPair != Registers.RegisterPairEncoding.DE)
                                IncorrectRegPairEncoding();
                            cycles = POP(regPair);
                            break;
                        case 2:
                            if (condition != ConditionFlags.NC)
                                IncorrectCondition();
                            cycles = Jcondition(condition);
                            break;
                        case 3:
                            cycles = OUT();
                            break;
                        case 4:
                            if (condition != ConditionFlags.NC)
                                IncorrectCondition();
                            cycles = Ccondition(condition);
                            break;
                        case 5:
                            if (regPair != Registers.RegisterPairEncoding.DE)
                                IncorrectRegPairEncoding();
                            cycles = PUSH(regPair);
                            break;
                        case 6:
                            cycles = SUI();
                            break;
                        case 7:
                            cycles = RST((byte)condition);
                            break;
                        case 8:
                            if (condition != ConditionFlags.C)
                                IncorrectCondition();
                            cycles = Rcondition(condition);
                            break;
                        case 9:
                            NotImplemented();
                            break;
                        case 0xA:
                            if (condition != ConditionFlags.C)
                                IncorrectCondition();
                            cycles = Jcondition(condition);
                            break;
                        case 0xB:
                            cycles = IN();
                            break;
                        case 0xC:
                            if (condition != ConditionFlags.C)
                                IncorrectCondition();
                            cycles = Ccondition(condition);
                            break;
                        case 0xD:
                            NotImplemented();
                            break;
                        case 0xE:
                            cycles = SBI();
                            break;
                        case 0xF:
                            cycles = RST((byte)condition);
                            break;
                    }
                    break;

                case 0xE:
                    switch (secondNibble)
                    {
                        case 0:
                            if (condition != ConditionFlags.PO)
                                IncorrectCondition();
                            cycles = Rcondition(condition);
                            break;
                        case 1:
                            if (regPair != Registers.RegisterPairEncoding.HL)
                                IncorrectRegPairEncoding();
                            cycles = POP(regPair);
                            break;
                        case 2:
                            if (condition != ConditionFlags.PO)
                                IncorrectCondition();
                            cycles = Jcondition(condition);
                            break;
                        case 3:
                            cycles = XTHL();
                            break;
                        case 4:
                            if (condition != ConditionFlags.PO)
                                IncorrectCondition();
                            cycles = Ccondition(condition);
                            break;
                        case 5:
                            if (regPair != Registers.RegisterPairEncoding.HL)
                                IncorrectRegPairEncoding();
                            cycles = PUSH(regPair);
                            break;
                        case 6:
                            cycles = ANI();
                            break;
                        case 7:
                            cycles = RST((byte)condition);
                            break;
                        case 8:
                            if (condition != ConditionFlags.PE)
                                IncorrectCondition();
                            cycles = Rcondition(condition);
                            break;
                        case 9:
                            cycles = PCHL();
                            break;
                        case 0xA:
                            if (condition != ConditionFlags.PE)
                                IncorrectCondition();
                            cycles = Jcondition(condition);
                            break;
                        case 0xB:
                            cycles = XCHG();
                            break;
                        case 0xC:
                            if (condition != ConditionFlags.PE)
                                IncorrectCondition();
                            cycles = Ccondition(condition);
                            break;
                        case 0xD:
                            NotImplemented();
                            break;
                        case 0xE:
                            cycles = XRI();
                            break;
                        case 0xF:
                            cycles = RST((byte)condition);
                            break;
                    }
                    break;

                case 0xF:
                    switch (secondNibble)
                    {
                        case 0:
                            if (condition != ConditionFlags.P)
                                IncorrectCondition();
                            cycles = Rcondition(condition);
                            break;
                        case 1:
                            cycles = POP();
                            break;
                        case 2:
                            if (condition != ConditionFlags.P)
                                IncorrectCondition();
                            cycles = Jcondition(condition);
                            break;
                        case 3:
                            cycles = DI();
                            break;
                        case 4:
                            if (condition != ConditionFlags.P)
                                IncorrectCondition();
                            cycles = Ccondition(condition);
                            break;
                        case 5:
                            cycles = PUSH();
                            break;
                        case 6:
                            cycles = ORI();
                            break;
                        case 7:
                            cycles = RST((byte)condition);
                            break;
                        case 8:
                            if (condition != ConditionFlags.M)
                                IncorrectCondition();
                            cycles = Rcondition(condition);
                            break;
                        case 9:
                            cycles = SPHL();
                            break;
                        case 0xA:
                            if (condition != ConditionFlags.M)
                                IncorrectCondition();
                            cycles = Jcondition(condition);
                            break;
                        case 0xB:
                            cycles = EI();
                            break;
                        case 0xC:
                            if (condition != ConditionFlags.M)
                                IncorrectCondition();
                            cycles = Ccondition(condition);
                            break;
                        case 0xD:
                            NotImplemented();
                            break;
                        case 0xE:
                            cycles = CPI();
                            break;
                        case 0xF:
                            cycles = RST((byte)condition);
                            break;
                    }
                    break;
            }
            if (cycles == 0)
                throw new Exception("0 Cycle");
            return cycles;
        }

        public void Step()
        {
            InstructionPipeline(EndianIO.ReadByte());
        }

        void NotImplemented()
        {
            throw new Exception("Opcode Not Implemented or Null");
        }

        void IncorrectRegPairEncoding()
        {
            throw new Exception("Reg Pair Encoding not correct for current address");
        }

        void IncorrectRegister()
        {
            throw new Exception("Reg Encoding not correct for current address");
        }

        void IncorrectCondition()
        {
            throw new Exception("Condition Flag not correct for current address");
        }

        void PrintDiag()
        {
            logStream.WriteLine();
            logStream.WriteLine("A: " + Registers.A.ToString("X2"));
            logStream.WriteLine("B: " + Registers.B.ToString("X2"));
            logStream.WriteLine("C: " + Registers.C.ToString("X2"));
            logStream.WriteLine("D: " + Registers.D.ToString("X2"));
            logStream.WriteLine("E: " + Registers.E.ToString("X2"));
            logStream.WriteLine("H: " + Registers.H.ToString("X2"));
            logStream.WriteLine("L: " + Registers.L.ToString("X2"));
            logStream.WriteLine("PC: " + Registers.PC.ToString("X4"));
            logStream.WriteLine("SP: " + Registers.SP.ToString("X4"));
            logStream.WriteLine("F: " + Convert.ToString(Registers.F.Flags, 2));
            logStream.WriteLine("S: " + Registers.F.S.ToString("X2"));
            logStream.WriteLine("Z: " + Registers.F.Z.ToString("X2"));
            logStream.WriteLine("AC: " + Registers.F.AC.ToString("X2"));
            logStream.WriteLine("P: " + Registers.F.P.ToString("X2"));
            logStream.WriteLine("CY: " + Registers.F.CY.ToString("X2"));
        }

        bool isBetweenIncluded(byte number, byte low, byte max)
        {
            if (number > low && number < max)
                return true;
            return false;
        }
    }
}
