using Intel_8080_MPU_Core.Units;
using System;
using Xunit;

namespace Intel_8080_MPU_Core_Tests
{
    public class RegistersTest
    {
        [Fact]
        public void GetBC()
        {
            Registers reg = new Registers(new BasicFlags())
            {
                B = 0x66,
                C = 0x73
            };
            Assert.Equal(0x6673, reg.BC);
        }
        [Fact]
        public void GetDE()
        {
            Registers reg = new Registers(new BasicFlags())
            {
                D = 0x66,
                E = 0x73
            };
            Assert.Equal(0x6673, reg.DE);
        }
        [Fact]
        public void GetHL()
        {
            Registers reg = new Registers(new BasicFlags())
            {
                H = 0x66,
                L = 0x73
            };
            Assert.Equal(0x6673, reg.HL);
        }
        [Fact]
        public void SetBC_GetB()
        {
            Registers reg = new Registers(new BasicFlags())
            {
                B = 0x36,
                C = 0x46,
                BC = 0x1278
            };
            Assert.Equal(0x12, reg.B);
        }
        [Fact]
        public void SetDE_GetD()
        {
            Registers reg = new Registers(new BasicFlags())
            {
                D = 0x36,
                E = 0x46,
                DE = 0x1278
            };
            Assert.Equal(0x12, reg.D);
        }
        [Fact]
        public void SetDE_GetE()
        {
            Registers reg = new Registers(new BasicFlags())
            {
                D = 0x36,
                E = 0x46,
                DE = 0x1278
            };
            Assert.Equal(0x78, reg.E);
        }
        [Fact]
        public void SetHL_GetH()
        {
            Registers reg = new Registers(new BasicFlags())
            {
                H = 0x36,
                L = 0x46,
                HL = 0x1278
            };
            Assert.Equal(0x12, reg.H);
        }
        [Fact]
        public void SetHL_GetL()
        {
            Registers reg = new Registers(new BasicFlags())
            {
                H = 0x36,
                L = 0x46,
                HL = 0x1278
            };
            Assert.Equal(0x78, reg.L);
        }
        [Fact]
        public void SetBC_GetC()
        {
            Registers reg = new Registers(new BasicFlags())
            {
                B = 0x63,
                C = 0x72,
                BC = 0x4637
            };
            Assert.Equal(0x37, reg.C);
        }
        [Fact]
        public void Set_S_Flag()
        {
            BasicFlags llf = new BasicFlags
            {
                S = 1
            };
            Assert.True(llf.S == 1);
        }
        [Fact]
        public void Reset_S_Flag()
        {
            BasicFlags llf = new BasicFlags
            {
                S = 0
            };
            Assert.False(llf.S == 1);
        }
        [Fact]
        public void Set_A_Flag()
        {
            BasicFlags llf = new BasicFlags
            {
                AC = 1
            };
            Assert.True(llf.AC == 1);
        }
        [Fact]
        public void Reset_A_Flag()
        {
            BasicFlags llf = new BasicFlags
            {
                AC = 0
            };
            Assert.False(llf.AC == 1);
        }
        [Fact]
        public void Set_All_Flag()
        {
            BasicFlags llf = new BasicFlags
            {
                AC = 1,
                CY = 1,
                P = 1,
                S = 1,
                Z = 1
            };
            Assert.Equal(0xD7, llf.Flags);
        }
        [Fact]
        public void Reset_All_Flag()
        {
            BasicFlags llf = new BasicFlags
            {
                AC = 0,
                CY = 0,
                P = 0,
                S = 0,
                Z = 0
            };
            Assert.Equal(0x2, llf.Flags);
        }
        [Fact]
        public void Rand_Flag()
        {
            BasicFlags llf = new BasicFlags
            {
                AC = 0,
                CY = 1,
                P = 1,
                S = 0,
                Z = 1
            };
            Assert.Equal(0x47, llf.Flags);
        }
        [Fact]
        public void Direct_Flag_Set()
        {
            BasicFlags llf = new BasicFlags
            {
                Flags = (byte)new Random(DateTime.Now.Second).Next(0, 255)
            };
            byte direct_AFlag = (byte)((llf.Flags & (int)BasicFlags.Masks.AC) >> (int)BasicFlags.Indexes.AC);
            Assert.Equal(direct_AFlag, llf.AC);
        }
        [Fact]
        public void ParityAndCarry()
        {
            BasicFlags llf = new BasicFlags()
            {
                CY = 1,
                P = 1
            };
            Assert.Equal(7, llf.Flags);
        }
        [Fact]
        public void OddParity()
        {
            BasicFlags llf = new BasicFlags();
            llf.Parity(7);
            Assert.False(llf.P == 1);
        }
        [Fact]
        public void EvenParity()
        {
            BasicFlags llf = new BasicFlags();
            llf.Parity(255);
            Assert.True(llf.P == 1);
        }
    }
}
