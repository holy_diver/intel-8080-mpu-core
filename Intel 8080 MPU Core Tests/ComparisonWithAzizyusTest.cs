﻿using Intel_8080_MPU_Core.Models;
using Intel_8080_MPU_Core.Units;
using Intel_8080_MPU_Core.Units.MPU8080;
using System;
using Xunit;
using Xunit.Abstractions;

namespace Intel_8080_MPU_Core_Tests
{
    public class ComparisonWithAzizyusTest
    {
        private readonly ITestOutputHelper output;
        MPU8080 intel;
        public ComparisonWithAzizyusTest(ITestOutputHelper output)
        {
            intel = new MPU8080(rand_64Kb_Mem(), new BasicFlags(), 256, false, false);
            this.output = output;
        }
        IRAM rand_64Kb_Mem()
        {
            Random rnd = new Random();
            RAM ram = new RAM(64 * 1024);
            for (ulong i = 0; i < ram.Size; i++)
            {
                ram.WriteByte(i, (byte)rnd.Next(0, 255));
            }
            return ram;
        }
        [Fact]
        public void EveryPossibleSBB_And_SUBTest()
        {
            MPU8080 integertel = new MPU8080(rand_64Kb_Mem(), new BasicFlags(), 256, false, false);
            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j < 256; j++)
                {
                    //SBB
                    intel.Registers.F.AC = integertel.Registers.F.AC = 0;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 1;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, integertel.Registers.F.CY);
                    intel.SBB(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 1;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 1;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, integertel.Registers.F.CY);
                    intel.SBB(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 0;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 0;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, integertel.Registers.F.CY);
                    intel.SBB(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 1;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 0;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, integertel.Registers.F.CY);
                    intel.SBB(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    //SUB
                    intel.Registers.F.AC = integertel.Registers.F.AC = 0;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 0;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, 0);
                    intel.SUB(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 1;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 1;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, 0);
                    intel.SUB(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 1;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 0;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, 0);
                    intel.SUB(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 0;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 1;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, 0);
                    intel.SUB(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);
                }
            }
        }

        [Fact]
        public void EveryPossibleADC_And_ADDTest()
        {
            MPU8080 integertel = new MPU8080(rand_64Kb_Mem(), new BasicFlags(), 256, false, false);
            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j < 256; j++)
                {
                    //ADC
                    intel.Registers.F.AC = integertel.Registers.F.AC = 0;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 1;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerAdd(integertel, integertel.Registers.F.CY);
                    intel.ADC(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 1;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 1;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerAdd(integertel, integertel.Registers.F.CY);
                    intel.ADC(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 0;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 0;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerAdd(integertel, integertel.Registers.F.CY);
                    intel.ADC(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 1;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 0;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerAdd(integertel, integertel.Registers.F.CY);
                    intel.ADC(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    //ADD
                    intel.Registers.F.AC = integertel.Registers.F.AC = 0;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 0;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerAdd(integertel, 0);
                    intel.ADD(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 1;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 1;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerAdd(integertel, 0);
                    intel.ADD(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 1;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 0;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerAdd(integertel, 0);
                    intel.ADD(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 0;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 1;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerAdd(integertel, 0);
                    intel.ADD(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.A, intel.Registers.A);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);
                }
            }
        }
        [Fact]
        public void EveryPossibleCMPTest()
        {
            MPU8080 integertel = new MPU8080(rand_64Kb_Mem(), new BasicFlags(), 256, false, false);
            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j < 256; j++)
                {
                    //CMP
                    intel.Registers.F.AC = integertel.Registers.F.AC = 0;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 0;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, 0);
                    intel.CMP(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 1;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 1;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, 0);
                    intel.CMP(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 1;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 0;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, 0);
                    intel.CMP(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);

                    intel.Registers.F.AC = integertel.Registers.F.AC = 0;
                    intel.Registers.F.CY = integertel.Registers.F.CY = 1;
                    intel.Registers.E = integertel.Registers.E = (byte)j;
                    intel.Registers.A = integertel.Registers.A = (byte)i;
                    integerSub(integertel, 0);
                    intel.CMP(Registers.RegisterEncoding.E);
                    Assert.Equal(integertel.Registers.F.CY, intel.Registers.F.CY);
                    Assert.Equal(integertel.Registers.F.AC, intel.Registers.F.AC);
                    Assert.Equal(integertel.Registers.F.S, intel.Registers.F.S);
                    Assert.Equal(integertel.Registers.F.Z, intel.Registers.F.Z);
                }
            }
        }

        void integerSub(MPU8080 machine, byte val)
        {
            byte data = machine.Registers.E;
            int newByte = ~data + 1 - val;
            int integerRes = machine.Registers.A + newByte;
            uint result = (uint)integerRes;
            machine.Registers.F.CY = (byte)((result & 0x100) >> 8);
            machine.Registers.F.AC = (byte)((~(machine.Registers.A ^ machine.Registers.E ^ integerRes) & 0x10) >> 4);
            machine.Registers.A += (byte)newByte;
            machine.Registers.F.S = (byte)((machine.Registers.A & 0x80) >> 7);
            machine.Registers.F.Z = (machine.Registers.A == 0) ? (byte)1 : (byte)0;
        }

        void integerAdd(MPU8080 machine, byte val)
        {
            byte data = machine.Registers.E;
            int integerRes = machine.Registers.A + data + val;
            uint result = (uint)integerRes;
            machine.Registers.F.AC = (byte)(((machine.Registers.A ^ data ^ integerRes) & 0x10) >> 4);
            machine.Registers.A += data;
            machine.Registers.A += val;
            machine.Registers.F.CY = (byte)((result & 0x100) >> 8);
            machine.Registers.F.S = (byte)((machine.Registers.A & 0x80) >> 7);
            machine.Registers.F.Z = (machine.Registers.A == 0) ? (byte)1 : (byte)0;
        }
    }
}
